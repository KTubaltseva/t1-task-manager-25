package ru.t1.ktubaltseva.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.ICommand;
import ru.t1.ktubaltseva.tm.api.service.IAuthService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IServiceLocator;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    public String getUserId() throws AuthRequiredException {
        return getAuthService().getUserId();
    }

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += ": " + description;
        return result;
    }

}
