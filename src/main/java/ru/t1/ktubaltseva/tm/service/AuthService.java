package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.service.IAuthService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.*;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, NoSuchAlgorithmException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user;
        try {
            user = userService.findByLogin(login);
        } catch (UserNotFoundException e) {
            throw new LoginIncorrectException();
        }
        final boolean locked = user.isLocked();
        if (locked) throw new UserIsLockedException();
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        if (!passwordHash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) throws UserNotFoundException, AbstractAuthException {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public String getUserId() throws AuthRequiredException {
        if (!isAuth()) throw new AuthRequiredException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() throws UserNotFoundException, AuthRequiredException {
        try {
            return userService.findOneById(userId);
        } catch (IdEmptyException e) {
            throw new AuthRequiredException();
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
    }

}
