package ru.t1.ktubaltseva.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.service.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(
            @NotNull final String value,
            @NotNull final String secret,
            @NotNull final Integer iteration
    ) throws NoSuchAlgorithmException {
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + value + secret);
        }
        return result;
    }

    @NotNull
    static String salt(
            @NotNull final ISaltProvider saltProvider,
            @NotNull final String value
    ) throws NoSuchAlgorithmException {
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @NotNull
    static String md5(@NotNull final String value) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        @NotNull final byte[] array = md.digest(value.getBytes());
        @NotNull final StringBuffer sb = new StringBuffer();
        for (byte b : array) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
        }
        return sb.toString();
    }

}
